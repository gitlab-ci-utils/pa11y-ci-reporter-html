# Version Upgrade Guide

## Upgrading from v3.x to v4.x

Starting with v4.0.0, the Pa11y CI HTML Reporter uses the new Pa11y CI reporter interface directly (added in Pa11y CI v3.0.0). It can no longer post-process a Pa11y CI JSON results file. The following changes are required:

- Upgrade to Pa11y CI v3.0.1 (or later). Version 3.0.1 is required to fix a bug affecting reporters with async functions.
- Any execution scripts and Pa11y CI configuration must be update as follows:
  - Creation of a JSON report file is no longer required (but can be included)
  - Remove the CLI execution of `pa11y-ci-reporter-html`
  - Update `pa11y-ci` configuration to specify the reporter (or specify via CLI per the [usage instructions](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html#usage))

For example, if the reporter was previously executed via the following commands:

```sh
pa11y-ci --json > pa11y-ci-results.json
pa11y-ci-reporter-html
```

Update the command to simply:

```sh
pa11y-ci
```

Update the Pa11y CI configuration file as specified below, which will generate both the JSON and HTML reports (as the previous commands did):

```json
{
  "defaults": {
    "reporters": [
      ["json", { "fileName": "pa11y-ci-results.json" }],
      "pa11y-ci-reporter-html"
    ]
  },
  "urls": [
    ...
  ]
}
```

As with previous versions, there are options to set the destination directory for the HTML reports and whether to include detailed page reports for pages with no Pa11y issues. These can be set in the Pa11y CI configuration file per the [usage instructions](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html#usage).
