# Pa11y CI HTML Reporter

Pa11y CI HTML Reporter generates HTML-formatted report files for
[Pa11y CI](https://github.com/pa11y/pa11y-ci) results.

## HTML reports

Pa11y CI HTML Reporter generates the following HTML report files:

- An `index.html` file displaying summary results for all analyzed URLs, with
  links to each URL report (if applicable). If Pa11y CI generates an error when
  analyzing the page (for example timeout, DNS lookup), the error is displayed
  instead of the summary.
- A detailed HTML report file for each analyzed URL with Pa11y issues. These
  reports are generated using the
  [`pa11y-reporter-html-plus`](https://www.npmjs.com/package/pa11y-reporter-html-plus)
  reporter, with file names generated from the URL. By default, these report
  files are only generated for pages with Pa11y issues, but there is a
  configuration option that can be used to override this and include reports
  for all pages (see [below](#using-via-configuration-file)).

An example report from Pa11y CI analysis of several pages from the
[W3C accessibility demo site](https://www.w3.org/WAI/demos/bad/) can be seen
[here](https://gitlab-ci-utils.gitlab.io/pa11y-ci-reporter-html/).

## Installation

Install Pa11y CI HTML Reporter via
[npm](https://www.npmjs.com/package/pa11y-ci-reporter-html).

```sh
npm install pa11y-ci-reporter-html
```

Note: `pa11y-ci` and `pa11y-ci-reporter-html` must either both be installed
globally or both be installed locally. Using a mixed configuration can result
in the reporter not being found.

## Usage

Pa11y CI HTML Reporter is a Pa11y CI compatible reporter. As with all
reporters, it can be specified via the CLI or a Pa11y CI configuration file.
Complete details on using reporters can be found in the
[Pa11y CI documentation](https://github.com/pa11y/pa11y-ci#reporters).

### Using via CLI

The reporter can be used with Pa11y CI via the CLI as follows:

```sh
pa11y-ci --reporter=pa11y-ci-reporter-html https://pa11y.org/
```

When specified via the CLI:

- Only one reporter can be used. If multiple reporters are required, they must
  be specified via a Pa11y CI configuration file.
- Reporter configuration options can't be specified and the default values are
  used (see [below](#using-via-configuration-file))
- If a reporter is specified via the CLI it overrides the Pa11y CI
  configuration file settings for reporters

### Using via configuration file

You can specify reporters in a Pa11y CI configuration file in the
`defaults.reporters` property (an array).

```json
{
  "defaults": {
    "reporters": ["pa11y-ci-reporter-html"]
  },
  "urls": ["https://pa11y.org/"]
}
```

The reporter can also be used with two optional configuration options:

- `destination`: A string specifying the directory for saving HTML report files
  (default: "./pa11y-ci-report"). Relative paths are resolved from the current
  working directory. The directory is created if it doesn't exist.
- `includeZeroIssues`: A Boolean value indicating whether to include detailed
  page reports for pages with no Pa11y issues (default: false)

In this case the reporter is specified as an array, with the first element a
string with the reporter name and the second element an object with the
configuration options.

```json
{
  "defaults": {
    "reporters": [
      [
        "pa11y-ci-reporter-html",
        { "destination": "./reports", "includeZeroIssues": true }
      ]
    ]
  },
  "urls": ["https://pa11y.org/"]
}
```

Multiple reporters can also be used as shown below:

```json
{
  "defaults": {
    "reporters": [
      "cli",
      ["pa11y-ci-reporter-html", { "destination": "./reports" }]
    ]
  },
  "urls": ["https://pa11y.org/"]
}
```
