'use strict';

/**
 * Process Pa11y CI results to generate HTML reports.
 *
 * @module reporter-html
 */
const fs = require('node:fs');
const path = require('node:path');
const handlebars = require('handlebars');
const logger = require('ci-logger');
const pageReporter = require('./page-reporter-html');

const summaryReportTemplateName = 'index.handlebars';
const summaryReportFileName = 'index.html';

/**
 * Generates a summary HTML report for all pages analyzed.
 *
 * @param {object} summary         The summary of Pa11y CI results for all pages.
 * @param {string} outputDirectory The directory to save the HTML report.
 * @static
 * @public
 */
const generateSummaryHtmlReport = (summary, outputDirectory) => {
    const templateFile = path.resolve(__dirname, summaryReportTemplateName);
    // File name specified above in configuration.
    // nosemgrep: eslint.detect-non-literal-fs-filename
    const summaryReportTemplate = fs.readFileSync(templateFile, 'utf8');
    const template = handlebars.compile(summaryReportTemplate);
    const summaryReport = template(summary);
    // File location specified by user input, allowed anywhere.
    // nosemgrep: path-join-resolve-traversal
    const outputFile = path.join(outputDirectory, summaryReportFileName);
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    fs.writeFileSync(outputFile, summaryReport);
};

/**
 * Check if pa11y results are a page error.
 *
 * @param   {Array}   issues Pa11y results issues array.
 * @returns {boolean}        True if results are a page error, otherwise false.
 * @static
 * @private
 */
const isPageError = (issues) =>
    issues.length === 1 && issues[0] instanceof Error;

/**
 * Puppeteer errors are returned in the format
 * "net::ERR_NAME_NOT_RESOLVED at https://this.url.does.not.exist/",
 * so extract error only and remove URL.
 *
 * @param   {Error}  error Page error.
 * @returns {string}       Formatted error string.
 * @static
 * @private
 */
const formatErrorMessage = (error) =>
    error.message.replace(/^(?<message>.*) at .*$/, '$1');

/**
 * Check if a page details report should be generated based on report options
 * and page results.
 *
 * @param   {object}  options    Reporter options.
 * @param   {object}  pageResult Pa11y results.
 * @returns {boolean}            True if a page detail report should be generated, otherwise false.
 * @static
 * @private
 */
const generatePageReport = (options, pageResult) =>
    (options && options.includeZeroIssues) || pageResult.issues.length > 0;

/**
 * Generates detailed HTML reports for each analyzed page.
 *
 * @param   {object}   pageResults Reformatted Pa11y CI results.
 * @param   {object}   options     Reporter configuration options.
 * @returns {object[]}             Array of page summary objects.
 * @static
 * @public
 */
const generatePageHtmlReports = async (pageResults, options) => {
    const pages = [];
    for (const pageResult of pageResults) {
        try {
            // If error then there will be only one element (an Error) in issues array
            if (isPageError(pageResult.issues)) {
                pages.push({
                    message: formatErrorMessage(pageResult.issues[0]),
                    url: pageResult.pageUrl
                });
            } else {
                const pageSummary = pageReporter.getPageSummary(pageResult);
                if (generatePageReport(options, pageResult)) {
                    pageSummary.htmlReport =
                        // eslint-disable-next-line no-await-in-loop -- Tracked in #115
                        await pageReporter.savePageHtmlReport(
                            pageResult,
                            options.destination
                        );
                }
                pages.push(pageSummary);
            }
        } catch (error) {
            logger.log({
                level: logger.Levels.Warn,
                message: `Error creating report for '${pageResult.pageUrl}' - ${error.message}`
            });
        }
    }
    return pages;
};

/**
 * Creates the specified output directory if it does not exist.
 *
 * @param {string} outputDirectory The output directory for the report.
 * @static
 * @public
 */
const ensureOutputDirectory = (outputDirectory) => {
    // File location specified by user input, allowed anywhere.
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    if (!fs.existsSync(outputDirectory)) {
        // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
        fs.mkdirSync(outputDirectory, { recursive: true });
    }
};

module.exports.generatePageHtmlReports = generatePageHtmlReports;
module.exports.generateSummaryHtmlReport = generateSummaryHtmlReport;
module.exports.ensureOutputDirectory = ensureOutputDirectory;
