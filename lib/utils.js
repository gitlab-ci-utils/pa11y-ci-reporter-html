'use strict';

/**
 * Miscellaneous utilities.
 *
 * @module utils
 */

const maxFilenameLength = 250;

/**
 * Generates a file name for the given URL, removing unnecessary information
 * (scheme, .html extension, etc) and replacing special characters with "-".
 *
 * @param   {string} url The URL to convert to a file name.
 * @returns {string}     The file name for the given URL.
 * @static
 */
const getFileNameFromUrl = (url) => {
    // Covers for all URL non-alphanumeric reserved/unreserved characters (for consistency) per
    // https://developers.google.com/maps/documentation/urls/url-encoding
    const result = url
        .replaceAll(/(?<url>^(?:https?|file):\/\/|\.html$)/g, '')
        .replaceAll(/[!#$%&'()*+,./:;=?@[\]_~]+/g, '-')
        .replace(/^-|-$/, '');
    return result.length <= maxFilenameLength
        ? result
        : result.slice(0, maxFilenameLength);
};

module.exports.getFileNameFromUrl = getFileNameFromUrl;
