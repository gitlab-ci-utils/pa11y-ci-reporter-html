'use strict';

module.exports = {
    destination: './pa11y-ci-report',
    includeZeroIssues: false
};
