'use strict';

/**
 * Reformats data.
 *
 * @module data-formatter
 */

/**
 * Reformats a Pa11y CI results object to facilitate generating HTML reports.
 *
 * @param   {object} results Pa11y CI results object.
 * @returns {object}         THe reformatted results object.
 * @static
 */
const getResultsForHtmlReport = (results) => ({
    results: Object.keys(results.results).map((key) => ({
        issues: results.results[key],
        pageUrl: key
    })),
    summary: {
        errors: results.errors,
        failures: results.total - results.passes,
        passes: results.passes,
        total: results.total
    }
});

module.exports.getResultsForHtmlReport = getResultsForHtmlReport;
