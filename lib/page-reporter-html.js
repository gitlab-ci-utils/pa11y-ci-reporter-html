'use strict';

/**
 * Process page results.
 *
 * @module page-reporter-html
 */

const fs = require('node:fs');
const path = require('node:path');
const pageReporter = require('pa11y-reporter-html-plus');
const utils = require('./utils');

/**
 * Generates an HTML report for a page and saves the file to the given directory.
 *
 * @param   {object} pageResult      Page result object.
 * @param   {string} outputDirectory The directory to save the HTML report.
 * @returns {string}                 The name of the HTML report file.
 * @static
 */
const savePageHtmlReport = async (pageResult, outputDirectory) => {
    // File name base generated internally.
    // nosemgrep: join_resolve_path_traversal
    const fileName = `${utils.getFileNameFromUrl(pageResult.pageUrl)}.html`;
    // nosemgrep: path-join-resolve-traversal
    const outputFile = path.join(outputDirectory, fileName);
    const htmlReport = await pageReporter.results(pageResult);
    // File location specified by user input, allowed anywhere.
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    fs.writeFileSync(outputFile, htmlReport);
    return fileName;
};

const getTypeCount = (issues, type) =>
    issues.filter((result) => result.type === type).length;

/**
 * Generates a summary of page results for use on the HTML summary report.
 *
 * @param   {object} pageResult Page result object.
 * @returns {object}            Summary of page results.
 * @static
 */
const getPageSummary = (pageResult) => ({
    issues: {
        errors: getTypeCount(pageResult.issues, 'error'),
        notices: getTypeCount(pageResult.issues, 'notice'),
        warnings: getTypeCount(pageResult.issues, 'warning')
    },
    url: pageResult.pageUrl
});

module.exports.savePageHtmlReport = savePageHtmlReport;
module.exports.getPageSummary = getPageSummary;
