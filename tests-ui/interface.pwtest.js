'use strict';

const { pathToFileURL } = require('node:url');
const { test, expect } = require('@playwright/test');

const reportFileName = './public/index.html';
const reportUrl = pathToFileURL(reportFileName).toString();

const pageCounts = {
    noIssues: 1,
    pa11yIssues: 3,
    pageErrors: 1
};
const totalPageCount =
    pageCounts.pa11yIssues + pageCounts.noIssues + pageCounts.pageErrors;

const pageRole = 'listitem';

const testCases = [
    {
        label: `Pa11y Issues`,
        locator: '.page.has-errors, .page.has-warnings, .page.has-notices',
        type: 'pa11yIssues'
    },
    {
        label: `No Issues`,
        locator:
            '.page:not(.has-errors, .has-warnings, .has-notices, .page-error)',
        type: 'noIssues'
    },
    {
        label: `Page Errors`,
        locator: '.page.page-error',
        type: 'pageErrors'
    }
];

test.describe('report filter tests', () => {
    test('should open with all results displayed', async ({ page }) => {
        await page.goto(reportUrl);
        await expect(page.getByRole(pageRole)).toHaveCount(totalPageCount);
    });

    for (const { type, label, locator } of testCases) {
        test(`should toggle ${label} pages when ${label} filter button clicked`, async ({
            page
        }) => {
            // Go to page and confirm that all page results are visible
            await page.goto(reportUrl);
            await expect(page.getByRole(pageRole)).toHaveCount(totalPageCount);

            // Apply filter then check that correct number of page results are
            // visible and filtered out page results are not visible.
            await page.getByText(label).click();
            await expect(page.getByRole(pageRole)).toHaveCount(
                totalPageCount - pageCounts[type]
            );
            await expect(
                // eslint-disable-next-line playwright/no-raw-locators -- no semantic locators
                page.locator(locator).locator('visible=true')
            ).toHaveCount(0);

            // Remove filter then check that all page results are visible.
            await page.getByText(label).click();
            await expect(page.getByRole(pageRole)).toHaveCount(totalPageCount);
        });
    }
});
