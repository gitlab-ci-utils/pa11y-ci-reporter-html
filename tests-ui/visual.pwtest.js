'use strict';

const { promisify } = require('node:util');
const exec = promisify(require('node:child_process').exec);
const { pathToFileURL } = require('node:url');
const { test, expect } = require('@playwright/test');

const reportFileName = './public/index.html';
const reportUrl = pathToFileURL(reportFileName).toString();

// Disable screenshot on failure since saved as part of screenshot test.
// Increased timeout to generate pages when not in CI.
// Disable trace since no interaction with the page.

test.use({ screenshot: 'off', timeout: 60 * 1000, trace: 'off' });

test.describe('visual regression tests', () => {
    test.beforeAll(async () => {
        // If running in CI, job needs the build_report artifacts,
        // so only need to generate report if not in CI.
        if (!process.env.CI) {
            // Pa11y-ci exits with error code, so catch failure.
            await exec(`npm run build-report && exit 1 || exit 0`);
        }
    });

    test('should match page screenshot', async ({ page }) => {
        await page.goto(reportUrl);

        // Update page date to a fix value to avoid date differences in screenshot
        await page.evaluate(() => {
            // eslint-disable-next-line no-undef -- Running in browser context
            const h2 = document.querySelector('h2');
            h2.textContent =
                'Generated at: Sun Nov 26 2023 14:18:43 GMT-0600 (Central Standard Time)';
        });

        // Allow 0.005% (about 4600 pixels) difference for rendering
        // variations. Had to expand a little for webkit variances.
        await expect(page).toHaveScreenshot({ maxDiffPixelRatio: 0.005 });
    });
});
