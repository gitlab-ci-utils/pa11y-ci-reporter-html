# Changelog

## v7.0.0 (2024-09-04)

### Changed

- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#127)
- BREAKING: Removed `pa11y-ci` from `peerDependencies` to facilitate use with
  forks. (#128)
  - A `supports` property was added to the reporter to indicate `pa11y-ci`
    compatibility, similar to the `pa11y` reporter interface (although not
    formally part of the `pa11y-ci` reporter interface).
- Updated HTML report to declare all colors via CSS custom properties
  (i.e. variables) and update CSS to use logic properties.
- Update to latest dependencies (`ci-logger@7.0.0`,
  `pa11y-reporter-html-plus@4.0.1`).

### Fixed

- Updated visual regression tests to replace report date with a fixed value to
  remove differences and adjusted pixel threshold. (#124)
- Increased visual regression test threshold to accommodate webkit rendering
  variances.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#126)
- Updated CI pipeline to have Pagean test local files rather than served pages,
  resolving issues failing to load `favicon.ico` in templates v32. Also updated
  broken links test to only fail on internal link failures (that is, between
  reports) and warn on external link failures.

## v6.0.2 (2023-08-14)

### Fixed

- Updated to latest dependencies (`pa11y-reporter-html-plus@2.0.1`).

## v6.0.1 (2023-08-13)

### Fixed

- Updated to latest dependencies (`handlebars@4.7.8`).

### Miscellaneous

- Updated documentation to clarify requirements on `pa11y-ci` and reporter
  installation location.
- Update all UI test image snapshots for latest OS. (#119)

## v6.0.0 (2023-06-09)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#113)
- Updated report to use system fonts. (#117)

### Fixed

- Updated to latest dependencies (`ci-logger@6.0.0`, `pa11y-reporter-html-plus@2.0.0`).
- Removed `rimraf` from tests. (#114)

### Miscellaneous

- Migrated Playwright tests from Ubuntu Focal to Jammy. (#116)

## v5.1.1 (2023-03-07)

### Fixed

- Fixed keyboard accessibility issues with report filter buttons. (#111)
- Updated to `pa11y-reporter-html-plus@1.1.1`, resolving several accessibility
  issues in the report.

## v5.1.0 (2023-02-28)

### Changed

- Updated summary report layout to use up to the maximum screen width to
  display long URLs. (#64)
- Updated to [`pa11y-reporter-html-plus@1.1.0`](https://gitlab.com/gitlab-ci-utils/pa11y-reporter-html-plus/-/releases/1.1.0),
  which adds buttons to accessibility issues to copy the selector to the
  clipboard.

### Fixed

- Updated all tests and examples to include the `axe` runner in addition to
  `htmlcs`. (#29)
- Removed old tests left over from before `reporters` interface was defined
  and this package post processed a file that could have errors. (#64)

### Miscellaneous

- Moved visual regression tests to Playwright and updated Renovate config to
  group NPM package and container image. (#108)
- Added Playwright UI test for report results filtering capabilities. (#104)

## v5.0.4 (2023-02-14)

### Fixed

- Update to `pa11y-reporter-html-plus@1.0.5`, resolving an issue when the
  `axe` runner reports a `null` value for `impact`.

## v5.0.3 (2023-02-05)

### Fixed

- Update to latest dependencies (`ci-logger@5.1.1`, `pa11y-reporter-html-plus@1.0.4`).
- Updated devDependency `serve-handler@6.1.5`, resolving [CVE-2022-3517](https://nvd.nist.gov/vuln/detail/CVE-2022-3517). (#106)
- Updated tests and CI pipeline for breaking changes in `puppeteer` v19.
- Updated tests for breaking changes in `jest` v29.

### Miscellaneous

- Updated pipeline to have long-running jobs use GitLab shared `medium` sized runners. (#105)
- Update `build_pages` job with retry due to transient network errors accessing W3C pages. (#107)

## v5.0.2 (2022-09-25)

### Fixed

- Updated to latest dependencies (`ci-logger@5.1.0`, `pa11y-reporter-html-plus@1.0.3`)

### Miscellaneous

- Replaced `del` with `rimraf` in integration tests. (#102)

## v5.0.1 (2022-07-17)

### Fixed

- Fixed filtering in the summary report for pages with only warnings and/or notices (no errors). (#101)
- Updated to latest dependencies.

### Miscellaneous

- Added logic in CI `build_report` job to check for the correct number of report files due to transient network errors causing external page loads to intermittently fail. (#100)

## v5.0.0 (2022-07-10)

### Changed

- BREAKING: Dropped support for Node 12 and 17 since end-of-life (#91, #92)
- BREAKING: Updated page reports to use [`pa11y-reporter-html-plus`](https://www.npmjs.com/package/pa11y-reporter-html-plus) instead of the built-in Pa11y HTML reporter. This includes enhanced support for all built-in Pa11y runners (`htmlcs`, `axe`), including links to relevant help, and the ability to filter accessibility issues by type. (#90)
- Updated the summary report format to align with the `pa11y-reporter-html-plus` page report format and added a run summary including pages analyzed, passed/failed, and total accessibility issues. (#73, #89, #98)

### Fixed

- Fixed handling of Puppeteer errors to remove URL since errors are already reported by URL (e.g. `net::ERR_NAME_NOT_RESOLVED at https://this.is.an.invalid.url` is now truncated to `net::ERR_NAME_NOT_RESOLVED`). (#78)
- Updated integration test timeouts to resolve Windows test timeout issues in CI. (#81)
- Updated to latest dependencies, including resolving CVE-2021-44906 (dev only).
- Updated `visual_test` CI job to not run on schedule for consistency with other tests. (#88)
- Updated `build_pages` CI job to only allow exit code 2 (a11y errors). (#99)
- Updated test fixtures to reflect the updated `pa11y-ci@5.3.0` format with `runner` and `runnerExtras` properties. (#28)
- Added required test coverage thresholds (#95)

### Miscellaneous

- Registered project for [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/5555/badge)](https://bestpractices.coreinfrastructure.org/projects/5555) badge
- Updated project policy documents (CODE_OF_CONDUCT.md, CONTRIBUTING.md, SECURITY.md). (#80)
- Added reporter [`pa11y-ci-reporter-cli-summary`](https://www.npmjs.com/package/pa11y-ci-reporter-cli-summary) to `pa11y-ci` CI job (#77)
- Refactored test to leverage [`pa11y-ci-reporter-runner`](https://www.npmjs.com/package/pa11y-ci-reporter-runner) (#82, #84)
- Added `prettier` and disabled `eslint` formatting rules. (#86)
- Fix package.json description to reflect reporter interface. (#87)

## v4.0.0 (2021-12-20)

### Changes

- BREAKING: Updated to use the Pa11y CI reporter interface, which also requires Pa11y CI v3.0.1 (or later). The CLI interface to post-process Pa11y CI JSON reports is no longer supported. See the [upgrade guide](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html/-/blob/master/docs/upgrade-guide.md) for migration details. (#53)
- Updated page report names to remove `file` scheme prefix (in addition to `http`/`https`) (#71)

### Fixed

- Updated to latest dependencies

## v3.0.3 (2021-12-03)

**Note: With the release of `pa11y-ci` v3.0.0, this package is being migrated to use the new reporter interface in v4.0.0. This is the last planned release with a CLI interface that supports generating reports from a `pa11y-ci` JSON results file.**

### Fixed

- Fixed issue with long URLs producing report filenames over 255 characters (#70) (thanks @pwmedcraft)
- Updated to latest dependencies

## v3.0.2 (2021-11-11)

### Fixed

- Fixed CSS error in HTML summary report ([!107](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html/-/merge_requests/107))
- Updated to latest dependencies
- Updated project linting rules, and refactored code as required (#67)

### Miscellaneous

- Minor optimizations to CI pipeline (#65, #66)

## v3.0.1 (2021-09-12)

### Fixed

- Updated to latest dependencies
- Fixed error messages in some unit tests to work with changes in Node 16.9 (#63)

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates (#59)

## v3.0.0 (2021-06-27)

### Changed

- BREAKING: Changed the default destination directory from the current working directory to `./pa11y-ci-report` to avoid creating potentially a large number of files in the current working directory. To revert to the previous behavior, run with the `-d ./` option. (#58)
- BREAKING: Deprecated support for Node 10, 13, and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#30, #50)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added visual regression tests of HTML report (#55)

## v2.1.4 (2021-05-15)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities
- Fixed error in invalid encoding test (#51)

### Miscellaneous

- Optimized published package to only include the minimum required files (#49)

## v2.1.3 (2021-03-27)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated CI pipeline to use standard NPM package collection (#47) and use `needs` instead of `dependencies` for efficiency (#48)

## v2.1.2 (2021-02-27)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities (#43)

### Miscellaneous

- Updated CI pipeline to Pagean 4.4 with pages served from a static server (#44), add new CSS linting rules (#45), and optimize for schedules (#46)

## v2.1.1 (2020-11-29)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#41) and GitLab Releaser (#42)
- Updated documentation for all exposed functions for consistency (#40)

## v2.1.0 (2020-10-19)

### Changed

- Added capability to read UTF-16LE encoded pa11y-ci JSON results file, which is the default format when piping output to a file in PowerShell 5.1. (#38)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Extracted CLI test module to new npm module [bin-tester](https://www.npmjs.com/package/bin-tester) (#39)

## v2.0.3 (2020-08-30)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v2.0.2 (2020-04-05)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities
- Updated to latest GitLab `license_scanning` job

## v2.0.1 (2020-03-22)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v2.0.0 (2020-01-22)

### Changed

- BREAKING: Changed default behavior to only generate page reports for analyzed URLs with pa11y issues, with command line option to override and generate page reports for all URLs (#32)
- BREAKING: Removed Node v8 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=13.0.0`) (#26, #30)

### Fixed

- Fixed line endings to resolve yarn installation issue (#33)
- Updated to latest dependencies

### Miscellaneous

- Updated project to use [eslint-plugin-sonarjs](https://www.npmjs.com/package/eslint-plugin-sonarjs) (#27)

## v1.2.4 (2019-11-24)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.3 (2019-11-17)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.2 (2019-11-03)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated CI pipeline to lint HTML and CSS
- Updated project to use custom eslint configuration module (#25)

## v1.2.1 (2019-10-20)

### Fixed

- Updated to latest dependencies

## v1.2.0 (2019-08-04)

### Changed

- Updated the summary report to better highlight pages with issues versus those with no issues. Also added filtering options for pages with pa11y issues, pages with no issues, and pages that pa11y failed to analyze. All cases are included in [the example report](https://gitlab-ci-utils.gitlab.io/pa11y-ci-reporter-html/)). (#16)

## v1.1.0 (2019-07-14)

### Changed

- Fixed bug where errors in pa11y-ci results caused report generation to fail. These errors are now included in the summary report (see [the example report](https://gitlab-ci-utils.gitlab.io/pa11y-ci-reporter-html/)). (#14)
- Updated summary report to show full report URL on hover to better handle truncated long names (#15)
- Improved validation of user input, error handling, and logging (#5, #11, #12)
- Updated testing to include CLI integration tests (#8) and `pa11y-ci` analysis of the sample report (#17)

### Documentation

- Updated readme to note that destination directory will be created if it does not exist (#13)

## v1.0.1 (2019-06-16)

### Documentation

- Moved repository to a new group and updated example location.

## v1.0.0 (2019-06-02)

Initial release
