/* eslint-disable sort-keys -- Match report */
'use strict';

module.exports = {
    summary: {
        total: 2,
        passes: 0,
        errors: 4
    },
    results: [
        {
            pageUrl: 'test1.html',
            issues: [
                {
                    code: 'WCAG2AA.Principle2.Guideline2_4.2_4_2.H25.2',
                    type: 'notice',
                    typeCode: 3,
                    message:
                        'Check that the title element describes the document.',
                    context:
                        '<title>Welcome to CityLights! [Inacces...</title>',
                    selector: 'html > head > title',
                    runner: 'htmlcs',
                    runnerExtras: {}
                },
                {
                    code: 'WCAG2AA.Principle3.Guideline3_1.3_1_1.H57.2',
                    type: 'error',
                    typeCode: 1,
                    message:
                        'The html element should have a lang or xml:lang attribute which describes the language of the document.',
                    context: '<html><head>\n<title>Welcome to CityLi...</html>',
                    selector: 'html',
                    runner: 'htmlcs',
                    runnerExtras: {}
                }
            ]
        },
        {
            pageUrl: 'test2.html',
            issues: [
                {
                    code: 'WCAG2AA.Principle2.Guideline2_4.2_4_4.H77,H78,H79,H80,H81',
                    type: 'notice',
                    typeCode: 3,
                    message:
                        'Check that the link text combined with programmatically determined link context identifies the purpose of the link.',
                    context:
                        '<a href="#page">Skip to inaccessible demo page</a>',
                    selector: '#skipnav > a',
                    runner: 'htmlcs',
                    runnerExtras: {}
                },
                {
                    code: 'WCAG2AA.Principle1.Guideline1_3.1_3_1.H48',
                    type: 'warning',
                    typeCode: 2,
                    message:
                        'If this element contains a navigation section, it is recommended that it be marked up as a list.',
                    context:
                        '<p id="logos"><a href="http://www.w3.org/" ti...</p>',
                    selector: '#logos',
                    runner: 'htmlcs',
                    runnerExtras: {}
                },
                {
                    code: 'WCAG2AA.Principle2.Guideline2_4.2_4_4.H77,H78,H79,H80,H81,H33',
                    type: 'notice',
                    typeCode: 3,
                    message:
                        'Check that the link text combined with programmatically determined link context, or its title attribute, identifies the purpose of the link.',
                    context:
                        '<a href="http://www.w3.org/" title="W3C Home"><img alt="W3C logo" src="../img...</a>',
                    selector: '#logos > a:nth-child(1)',
                    runner: 'htmlcs',
                    runnerExtras: {}
                }
            ]
        },
        {
            pageUrl: 'test3.html',
            issues: []
        },
        {
            pageUrl: 'page-error.html',
            issues: [new Error('Pa11y timed out (30000ms)')]
        }
    ]
};
