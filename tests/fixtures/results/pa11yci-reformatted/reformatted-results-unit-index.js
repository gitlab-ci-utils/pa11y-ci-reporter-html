/* eslint-disable sort-keys -- match report */
'use strict';

module.exports = {
    summary: {
        total: 3,
        passes: 1,
        errors: 2
    },
    results: [
        {
            pageUrl: 'somewhere.html',
            issues: [new Error('This is an error')]
        },
        {
            pageUrl: 'somewhere-else.html',
            issues: []
        },
        {
            pageUrl: 'somewhere-bad.html',
            issues: [
                {
                    code: 'WCAG2AA.Principle3.Guideline3_1.3_1_1.H57.2',
                    type: 'error',
                    typeCode: 1,
                    message:
                        'The html element should have a lang or xml:lang attribute which describes the language of the document.',
                    context: '<html><head>\n<title>Welcome to CityLi...</html>',
                    selector: 'html',
                    runner: 'htmlcs',
                    runnerExtras: {}
                },
                {
                    code: 'WCAG2AA.Principle1.Guideline1_1.1_1_1.H37',
                    type: 'error',
                    typeCode: 1,
                    message:
                        'Img element missing an alt attribute. Use the alt attribute to specify a short text alternative.',
                    context:
                        '<img src="./img/border_left_top.gif" width="10px" height="10px">',
                    selector:
                        '#page > table > tbody > tr > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > img',
                    runner: 'htmlcs',
                    runnerExtras: {}
                }
            ]
        }
    ]
};
