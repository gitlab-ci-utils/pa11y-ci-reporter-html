'use strict';

const fs = require('node:fs');
const path = require('node:path');
const { promisify } = require('node:util');
const exec = promisify(require('node:child_process').exec);
const defaultOptions = require('../lib/default-options');
const { startServer } = require('./helpers/http-server');

const testConfigOptionsFileName =
    './tests/fixtures/configs/test-options.pa11yci.json';
const testConfigNoOptionsFileName =
    './tests/fixtures/configs/test-no-options.pa11yci.json';
const summaryReportFileName = 'index.html';
const pageReportFileNames = [
    {
        url: 'localhost-3000-page1-with-errors.html',
        zeroIssues: false
    },
    {
        url: 'localhost-3000-page1-no-errors.html',
        zeroIssues: true
    }
];

const getExpectedFileList = (options) => {
    const expectedPageReports = pageReportFileNames
        .filter(
            (file) =>
                file.zeroIssues === false ||
                file.zeroIssues === options.includeZeroIssues
        )
        .map((file) => file.url);
    return [summaryReportFileName, ...expectedPageReports];
};

// Assumes reporter config has options, since only used to test options
const getReportOptions = (configFileName) => {
    // eslint-disable-next-line node/global-require -- mix of js and json files
    const configFile = require(configFileName);
    return configFile.defaults.reporters[0][1];
};

// Get command to execute package CLI command. Since yarn PnP doesn't
// use node_modules npx raises a warning and the test fails, so ensure
// it's run with yarn run.
const getExecCommand = () => {
    if (fs.existsSync(path.resolve(process.cwd(), '.pnp.cjs'))) {
        return 'yarn run';
    }
    return 'npx';
};

const nodeMajorVersion = Number.parseInt(
    process.version.split('.')[0].replace('v', '')
);

// Longer timeout needed when generating report (default 5s).
// Increased to 120s since 60s was timing out for Windows CI jobs.
const reportTimeout = 120_000;
jest.setTimeout(reportTimeout);

const testReporterWithOptions = async (options, command, assertions) => {
    expect.assertions(nodeMajorVersion > 21 ? assertions - 1 : assertions);

    // Spawn process to execute command to generate report
    const { stderr } = await exec(command);

    // Check that no errors were annunciated, but not in Node 21 where
    // there is the warning: "[DEP0040] DeprecationWarning: The `punycode`
    // module is deprecated."
    if (nodeMajorVersion < 21) {
        expect(stderr).toBe('');
    }

    // Check that all expected files were created
    const expectedFiles = getExpectedFileList(options);
    for (const file of expectedFiles) {
        expect(fs.existsSync(path.join(options.destination, file))).toBe(true);
    }

    // Check contents of summary report to ensure expected results
    // are passed to the reporter
    const summaryPage = fs.readFileSync(
        path.join(options.destination, summaryReportFileName),
        'utf8'
    );
    // There are two errors on the page (color contrast and image alt text), but caught by both runners.
    expect(summaryPage).toMatch(
        /(<li).*(http:\/\/localhost:3000\/page1-with-errors).*(4 errors).*(0 warnings).*(0 notices).*(<\/li>)/s
    );
    expect(summaryPage).toMatch(
        /(<li).*(http:\/\/localhost:3000\/page1-no-errors).*(0 errors).*(0 warnings).*(0 notices).*(<\/li>)/s
    );
    expect(summaryPage).toMatch(
        /(<li).*(https:\/\/pa11y.org\/timed-out.html).*(Pa11y timed out).*(<\/li>)/s
    );

    // Delete folder and all files created in this test
    fs.rmSync(options.destination, { force: true, recursive: true });
};

describe('pa11y-ci HTML reporter', () => {
    // eslint-disable-next-line jest/no-done-callback -- startServer API
    beforeAll((done) => {
        // eslint-disable-next-line promise/prefer-await-to-callbacks -- startServer API
        startServer((error, server) => {
            if (!error) {
                globalThis.server = server;
            }
            done(error);
        });
    });

    afterAll(() => {
        if (globalThis.server) {
            globalThis.server.close();
        }
    });

    it('should create report with default options if reporter specified in CLI', async () => {
        expect.hasAssertions();
        const options = defaultOptions;
        const command = `${getExecCommand()} pa11y-ci -c ${testConfigOptionsFileName} --reporter "./index.js" && exit 1 || exit 0`;

        await testReporterWithOptions(options, command, 6);
    });

    it('should create report with default options if none specified in config', async () => {
        expect.hasAssertions();
        const options = defaultOptions;
        const command = `${getExecCommand()} pa11y-ci -c ${testConfigNoOptionsFileName} && exit 1 || exit 0`;

        await testReporterWithOptions(options, command, 6);
    });

    it('should create report with options specified in config', async () => {
        expect.hasAssertions();
        const configFileName = testConfigOptionsFileName;
        // GetReport options needs the file name relative to this file, so remove current directory
        const options = getReportOptions(configFileName.replace('/tests', ''));
        const command = `${getExecCommand()} pa11y-ci -c ${configFileName} && exit 1 || exit 0`;

        await testReporterWithOptions(options, command, 7);
    });
});
