'use strict';

const fs = require('node:fs');
const path = require('node:path');
const handlebars = require('handlebars');
const logger = require('ci-logger');
const reporter = require('../lib/reporter-html');
const pageReporter = require('../lib/page-reporter-html');

describe('generate summary HTML report', () => {
    const outputDirectory = './foo';
    const summaryReportFileName = path.join(outputDirectory, 'index.html');
    const summaryData = JSON.parse(
        fs.readFileSync('./tests/fixtures/results/summary/summary-results.json')
    );
    const echoInput = (input) => input;
    let fsReadSpy, fsWriteSpy, handlebarsSpy;

    beforeAll(() => {
        // No need to mock read as the spy captures arguments and return values for test.
        // If mocked, causes issues with Yarn 2 PnP.
        fsReadSpy = jest.spyOn(fs, 'readFileSync');
        fsWriteSpy = jest
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});

        // Handlebars.compile returns a function that will be passed the summary data, so
        // simply return the input data to confirm that's what's being saved to file.
        handlebarsSpy = jest
            .spyOn(handlebars, 'compile')
            .mockImplementation(() => echoInput);

        reporter.generateSummaryHtmlReport(summaryData, outputDirectory);
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });

    it('should read the handlebars report template', () => {
        expect.assertions(1);
        expect(fsReadSpy.mock.calls[0][0]).toMatch(/.*\.handlebars/);
    });

    it('should compile the handlebars report template', () => {
        expect.assertions(1);
        expect(handlebarsSpy).toHaveBeenCalledWith(
            // Should be called with template read from file
            fsReadSpy.mock.results[0].value
        );
    });

    it('should save report file with the handlebars output', () => {
        expect.assertions(1);
        expect(fsWriteSpy).toHaveBeenCalledWith(
            summaryReportFileName,
            summaryData
        );
    });
});

describe('generate page html reports', () => {
    const generatePageHtmlReportsWithMocks = async (
        options,
        pageSummaryData
    ) => {
        // For the given test case, the resulting HTML report is the same as the page URL
        const pageHtmlReportSpy = jest
            .spyOn(pageReporter, 'savePageHtmlReport')
            .mockImplementation((page) => page.pageUrl);

        const pageSummary = await reporter.generatePageHtmlReports(
            pageSummaryData.results,
            options
        );

        return {
            pageHtmlReportSpy,
            pageSummary
        };
    };

    const destination = './foo';
    const optionsDefault = { destination, includeZeroIssues: false };
    const optionsWithZeroIssues = { destination, includeZeroIssues: true };

    // Expected summary results for pageSummaryData with optionsDefault
    const summaryDataFileDefault =
        './tests/fixtures/results/summary/summary-results-mix-page-errors.json';
    // Expected summary results for pageSummaryDataPuppeteer with optionsWithZeroIssues
    const summaryDataFileIncludeZero =
        './tests/fixtures/results/summary/summary-results-mix-page-errors-puppeteer-includezero.json';

    // eslint-disable-next-line node/global-require -- js file, required in test context
    const pageSummaryData = require('./fixtures/results/pa11yci-reformatted/reformatted-results-test-mix-page-errors');
    // eslint-disable-next-line node/global-require -- js file, required in test context
    const pageSummaryDataPuppeteer = require('./fixtures/results/pa11yci-reformatted/reformatted-results-test-mix-page-errors-puppeteer');
    // The case includes one url with invalid JSON for one error, so should fail
    const pageSummaryDataInvalidResults = JSON.parse(
        fs.readFileSync(
            './tests/fixtures/results/pa11yci-reformatted/reformatted-results-test-invalid-pa11yci.json'
        )
    );

    describe('with default options', () => {
        let pageHtmlReportSpy, pageSummary;

        beforeAll(async () => {
            ({ pageSummary, pageHtmlReportSpy } =
                await generatePageHtmlReportsWithMocks(
                    optionsDefault,
                    pageSummaryData
                ));
        });

        afterAll(() => {
            jest.restoreAllMocks();
        });

        describe('page reports', () => {
            it('should be generated for all pages with pa11y issues', () => {
                expect.assertions(3);
                const pageReportCount = 2;
                expect(pageHtmlReportSpy).toHaveBeenCalledTimes(
                    pageReportCount
                );
                expect(pageHtmlReportSpy).toHaveBeenCalledWith(
                    pageSummaryData.results[0],
                    destination
                );
                expect(pageHtmlReportSpy).toHaveBeenCalledWith(
                    pageSummaryData.results[1],
                    destination
                );
            });

            it('should not be generated for pages with no pa11y issues', () => {
                expect.assertions(1);
                expect(pageHtmlReportSpy).not.toHaveBeenCalledWith(
                    pageSummaryData.results[2],
                    destination
                );
            });

            it('should not be generated for pages with page errors', () => {
                expect.assertions(1);
                expect(pageHtmlReportSpy).not.toHaveBeenCalledWith(
                    pageSummaryData.results[3],
                    destination
                );
            });
        });

        it('summary results should be correct for all pages without puppeteer error', () => {
            expect.assertions(1);
            const { pages } = JSON.parse(
                fs.readFileSync(summaryDataFileDefault)
            );
            // This checks page counts, link to page details report, and page error handling
            expect(pageSummary).toStrictEqual(pages);
        });
    });

    describe('with include zero issues option', () => {
        let pageHtmlReportSpy, pageSummary;

        beforeAll(async () => {
            ({ pageSummary, pageHtmlReportSpy } =
                await generatePageHtmlReportsWithMocks(
                    optionsWithZeroIssues,
                    // Run run puppeteer error to test message reformatting
                    pageSummaryDataPuppeteer
                ));
        });

        afterAll(() => {
            jest.restoreAllMocks();
        });

        describe('page reports', () => {
            it('should be generated for all pages without page errors', () => {
                expect.assertions(4);
                const pageReportCount = 3;
                expect(pageHtmlReportSpy).toHaveBeenCalledTimes(
                    pageReportCount
                );
                expect(pageHtmlReportSpy).toHaveBeenCalledWith(
                    pageSummaryData.results[0],
                    destination
                );
                expect(pageHtmlReportSpy).toHaveBeenCalledWith(
                    pageSummaryData.results[1],
                    destination
                );
                expect(pageHtmlReportSpy).toHaveBeenCalledWith(
                    pageSummaryData.results[2],
                    destination
                );
            });

            // Repeat test with different options
            it('should not be generated for pages with page errors', () => {
                expect.assertions(1);
                expect(pageHtmlReportSpy).not.toHaveBeenCalledWith(
                    pageSummaryData.results[3],
                    destination
                );
            });
        });

        it('summary results should be correct for all pages with reformatted puppeteer error', () => {
            expect.assertions(1);
            const { pages } = JSON.parse(
                fs.readFileSync(summaryDataFileIncludeZero)
            );
            // This checks page counts, link to page details report, and page error handling
            expect(pageSummary).toStrictEqual(pages);
        });
    });

    describe('invalid pa11yci results', () => {
        let loggerSpy, pageSummary;

        beforeAll(async () => {
            loggerSpy = jest.spyOn(logger, 'log').mockImplementation(() => {});

            pageSummary = await reporter.generatePageHtmlReports(
                pageSummaryDataInvalidResults.results,
                optionsDefault
            );
        });

        afterAll(() => {
            jest.restoreAllMocks();
        });

        it('should log error for invalid pa11y-ci results', () => {
            expect.assertions(1);
            expect(loggerSpy).toHaveBeenCalledWith(
                expect.objectContaining({
                    level: logger.Levels.Warn,
                    message: expect.any(String)
                })
            );
        });

        it('should return valid results array', () => {
            expect.assertions(1);
            // Since only one url that had invalid data, should get empty results
            expect(pageSummary).toStrictEqual([]);
        });
    });
});

describe('ensure output directory', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should create dir that does not exist', () => {
        expect.assertions(1);
        const outputDirectory = 'foo/';
        const fsMkdirSpy = jest
            .spyOn(fs, 'mkdirSync')
            .mockImplementation(() => {});

        reporter.ensureOutputDirectory(outputDirectory);

        expect(fsMkdirSpy).toHaveBeenCalledWith(
            outputDirectory,
            expect.objectContaining({})
        );
    });

    it('should not create dir that does exist', () => {
        expect.assertions(1);
        const outputDirectory = './';
        const fsMkdirSpy = jest
            .spyOn(fs, 'mkdirSync')
            .mockImplementation(() => {});

        reporter.ensureOutputDirectory(outputDirectory);

        expect(fsMkdirSpy).not.toHaveBeenCalled();
    });
});
