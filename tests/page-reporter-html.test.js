'use strict';

const fs = require('node:fs');
const path = require('node:path');
const pa11yPageReporter = require('pa11y-reporter-html-plus');
const pageReporter = require('../lib/page-reporter-html');
const utils = require('../lib/utils');

const pageResultsFileName = 'reformatted-results-test.json';
const invalidPageResultFileError = 'Cannot read prop';

const getResults = (fileName) =>
    // eslint-disable-next-line node/global-require -- mix of js and json files
    require(`./fixtures/results/pa11yci-reformatted/${fileName}`);
describe('save page HTML report', () => {
    const outputDirectoryDefault = './';

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should get file name, generate page HTML report, save report, and return file name', async () => {
        expect.assertions(5);
        const results = getResults(pageResultsFileName);
        const result = results.results[0];
        const fileNameBase = 'foo-bar';
        const fileName = `${fileNameBase}.html`;
        const outputFile = path.join(outputDirectoryDefault, fileName);
        const htmlReport = 'This is a test';

        const utilsSpy = jest
            .spyOn(utils, 'getFileNameFromUrl')
            .mockImplementation(() => fileNameBase);
        const pa11yPageReporterSpy = jest
            .spyOn(pa11yPageReporter, 'results')
            .mockResolvedValue(htmlReport);
        const fsSpy = jest
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});

        const returnedFileName = await pageReporter.savePageHtmlReport(
            result,
            outputDirectoryDefault
        );

        expect(utilsSpy).toHaveBeenCalledWith(result.pageUrl);
        expect(pa11yPageReporterSpy).toHaveBeenCalledWith(result);
        expect(fsSpy).toHaveBeenCalledWith(outputFile, htmlReport);
        expect(returnedFileName).toStrictEqual(fileName);
        expect(outputFile).toMatch(new RegExp(`.*${fileName}`));
    });

    it('should throw when passed an invalid page result', async () => {
        expect.assertions(1);
        const invalidPageResult = {
            issues: [0, 1, 2],
            url: 'foo.html'
        };
        await expect(
            pageReporter.savePageHtmlReport(
                invalidPageResult,
                outputDirectoryDefault
            )
        ).rejects.toThrow(invalidPageResultFileError);
    });

    const testInvalidOutputDirectory = async (outputDirectory, error) => {
        const results = getResults(pageResultsFileName);
        const result = results.results[0];
        return expect(
            pageReporter.savePageHtmlReport(result, outputDirectory)
        ).rejects.toThrow(error);
    };

    it('should throw when passed an undefined output dir', () => {
        expect.assertions(1);
        testInvalidOutputDirectory(undefined, '"path" argument');
    });

    it('should throw when passed a non-string output dir', () => {
        expect.assertions(1);
        testInvalidOutputDirectory({}, '"path" argument');
    });
});

describe('get page summary', () => {
    const getPageSummary = (page) => {
        const results = getResults(pageResultsFileName);
        return pageReporter.getPageSummary(results.results[page]);
    };

    it('should return a properly formatted page summary', () => {
        expect.assertions(1);
        const summary = getPageSummary(0);
        expect(summary).toStrictEqual(
            expect.objectContaining({
                issues: expect.objectContaining({
                    errors: expect.any(Number),
                    notices: expect.any(Number),
                    warnings: expect.any(Number)
                }),
                url: expect.anything()
            })
        );
    });

    it('should return the correct summary for the first test page', () => {
        expect.assertions(1);
        const summary = getPageSummary(0);
        expect(summary).toMatchSnapshot();
    });

    it('should return the correct summary for the second test page', () => {
        expect.assertions(1);
        const summary = getPageSummary(1);
        expect(summary).toMatchSnapshot();
    });

    it('should throw when passed an undefined page result', () => {
        expect.assertions(1);
        expect(() => pageReporter.getPageSummary()).toThrow(
            invalidPageResultFileError
        );
    });

    it('should throw when passed an empty page result', () => {
        expect.assertions(1);
        expect(() => pageReporter.getPageSummary({})).toThrow(
            invalidPageResultFileError
        );
    });

    it('should throw when passed an invalid page result', () => {
        expect.assertions(1);
        const invalidPageResult = {
            issues: [0, 1, 2],
            url: 'foo.html'
        };
        expect(() =>
            pageReporter.getPageSummary({ invalidPageResult })
        ).toThrow(invalidPageResultFileError);
    });
});
