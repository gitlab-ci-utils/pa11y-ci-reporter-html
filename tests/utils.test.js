'use strict';

const utils = require('../lib/utils');

describe('get file name from URL', () => {
    const verifyGetFileNameFromUrl = (url, result) => {
        const fileName = utils.getFileNameFromUrl(url);
        expect(fileName).toStrictEqual(result);
    };

    it('should remove http prefix', () => {
        expect.assertions(1);
        const url = 'http://foo.com';
        const result = 'foo-com';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove https prefix', () => {
        expect.assertions(1);
        const url = 'https://foo.com';
        const result = 'foo-com';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove file prefix', () => {
        expect.assertions(1);
        const url = 'file://some/path/foo.txt';
        const result = 'some-path-foo-txt';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should not remove https in the middle of a URL', () => {
        expect.assertions(1);
        const url = 'https://foo.com/?redirect=https://bar.com';
        const result = 'foo-com-redirect-https-bar-com';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove .html suffix', () => {
        expect.assertions(1);
        const url = 'http://foo.com/index.html';
        const result = 'foo-com-index';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should not remove html in the middle of a URL', () => {
        expect.assertions(1);
        const url = 'http://foo.com/index.html?foo=bar';
        const result = 'foo-com-index-html-foo-bar';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should replace all special characters with a dash', () => {
        expect.assertions(1);
        const url = "_.~!*'();:@&=+$,/?%#[]";
        const result = '';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should replace multiple special characters in a row with one dash', () => {
        expect.assertions(1);
        const url =
            "http://foo.com/a_b.c~d!e*f'g(h)i;j:k@l&m=n+o$p,q/r?s%t#u[v]w";
        const result = 'foo-com-a-b-c-d-e-f-g-h-i-j-k-l-m-n-o-p-q-r-s-t-u-v-w';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove leading dash', () => {
        expect.assertions(1);
        const url = './foo.html';
        const result = 'foo';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove trailing dash', () => {
        expect.assertions(1);
        const url = 'http://foo.com/';
        const result = 'foo-com';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should truncate URL if it is over 250 characters', () => {
        expect.assertions(1);
        const url =
            'https://www.google.com/search?q=test+with+a+large+url&ie=UTF-8&largeUrlPadding=abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz012345678912345';
        const result =
            'www-google-com-search-q-test-with-a-large-url-ie-UTF-8-largeUrlPadding-abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz0123456789abcdefchijklmnopqrstuvwxyz012345678';
        verifyGetFileNameFromUrl(url, result);
    });
});
