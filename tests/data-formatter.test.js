'use strict';

const formatter = require('../lib/data-formatter');

describe('data formatter', () => {
    const checkValidResultObject = (fileName) => {
        // eslint-disable-next-line node/global-require -- mix of js and json files
        const results = require(fileName);
        const formattedResults = formatter.getResultsForHtmlReport(results);
        expect(formattedResults).toStrictEqual(
            expect.objectContaining({
                results: expect.arrayContaining([
                    expect.objectContaining({
                        issues: expect.arrayContaining([]),
                        pageUrl: expect.anything()
                    })
                ]),
                summary: {
                    errors: results.errors,
                    failures: results.total - results.passes,
                    passes: results.passes,
                    total: results.total
                }
            })
        );
    };

    it('should return a valid results object for results with no errors', () => {
        expect.assertions(1);
        const resultsFileName =
            './fixtures/results/pa11yci/pa11y-ci-results-E-no-error.json';
        checkValidResultObject(resultsFileName);
    });

    it('should return a valid results object for results with a mix of errors', () => {
        expect.assertions(1);
        const resultsFileName =
            './fixtures/results/pa11yci/pa11y-ci-results-E-mix-error.json';
        checkValidResultObject(resultsFileName);
    });

    it('should return a valid results object for results with all errors', () => {
        expect.assertions(1);
        const resultsFileName =
            './fixtures/results/pa11yci/pa11y-ci-results-EWN-all-error.json';
        checkValidResultObject(resultsFileName);
    });
});
