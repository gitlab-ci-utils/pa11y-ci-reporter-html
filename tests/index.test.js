'use strict';

const formatter = require('../lib/data-formatter');
const reporterFactory = require('../');
const reporterHtml = require('../lib/reporter-html');
const defaultOptions = require('../lib/default-options');
const { createRunner, RunnerStates } = require('pa11y-ci-reporter-runner');

// Unit test results - pa11yci, reformatted, and summary
// Reporter runner expects file name relative to cwd for tests
const pa11yciResultsFileName =
    './tests/fixtures/results/pa11yci/pa11y-ci-results-unit-index.json';
const formattedResults = require('./fixtures/results/pa11yci-reformatted/reformatted-results-unit-index');
const pageSummary = require('./fixtures/results/summary/summary-results-unit-index.json');

describe('reporter module', () => {
    it('should export a factory function that returns a reporter', () => {
        expect.assertions(3);

        expect(typeof reporterFactory).toBe('function');

        const reporter = reporterFactory();

        expect(typeof reporter).toBe('object');
        expect(typeof reporter.afterAll).toBe('function');
    });
});

describe('html reporter', () => {
    const optionsDestination = {
        destination: 'foo'
    };

    const optionsZeroIssues = {
        includeZeroIssues: true
    };

    const runReporterWithMocks = async (config, throwError = false) => {
        const consoleWarnSpy = jest
            .spyOn(console, 'warn')
            .mockImplementation(() => {});
        const ensureOutputSpy = jest
            .spyOn(reporterHtml, 'ensureOutputDirectory')
            .mockImplementation(() => {});
        const formatterSpy = jest
            .spyOn(formatter, 'getResultsForHtmlReport')
            .mockImplementation(() => {
                if (throwError) {
                    throw new Error('Ouch');
                } else {
                    return formattedResults;
                }
            });
        const generatePageReportSpy = jest
            .spyOn(reporterHtml, 'generatePageHtmlReports')
            .mockImplementation(() => pageSummary);
        const generateSummaryReportSpy = jest
            .spyOn(reporterHtml, 'generateSummaryHtmlReport')
            .mockImplementation(() => {});

        // Runner expects reporter name relative to cwd for tests
        const runner = createRunner(pa11yciResultsFileName, './', config);
        // Since afterAll is the only reporter method implemented, run until
        // that point, then clear mocks, and the run afterAll to ensure all
        // actions are done there.
        await runner.runUntilNext(RunnerStates.afterAll);
        jest.clearAllMocks();
        await runner.runNext();

        return {
            consoleWarnSpy,
            ensureOutputSpy,
            formatterSpy,
            generatePageReportSpy,
            generateSummaryReportSpy
        };
    };

    const checkOptions = async (options, property, expectedValue) => {
        expect.assertions(1);

        const { generatePageReportSpy } = await runReporterWithMocks(options);

        const actualOptions = generatePageReportSpy.mock.calls[0][1];

        expect(actualOptions[property]).toBe(expectedValue);
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    describe('options', () => {
        it('should use destination from options if provided', async () => {
            expect.hasAssertions();
            await checkOptions(
                optionsDestination,
                'destination',
                optionsDestination.destination
            );
        });

        it('should use default destination if none is provided in options', async () => {
            expect.hasAssertions();
            await checkOptions(
                optionsZeroIssues,
                'destination',
                defaultOptions.destination
            );
        });

        it('should use includeZeroIssues from options if provided', async () => {
            expect.hasAssertions();
            await checkOptions(
                optionsZeroIssues,
                'includeZeroIssues',
                optionsZeroIssues.includeZeroIssues
            );
        });

        it('should use default includeZeroIssues if none is provided in options', async () => {
            expect.hasAssertions();
            await checkOptions(
                optionsDestination,
                'includeZeroIssues',
                defaultOptions.includeZeroIssues
            );
        });
    });

    describe('reporter', () => {
        it('should check for destination directory', async () => {
            expect.assertions(2);

            const { ensureOutputSpy } =
                await runReporterWithMocks(optionsDestination);

            expect(ensureOutputSpy).toHaveBeenCalledTimes(1);
            expect(ensureOutputSpy).toHaveBeenCalledWith(
                optionsDestination.destination
            );
        });

        it('should reformat test results', async () => {
            expect.assertions(2);

            const { formatterSpy } = await runReporterWithMocks();

            expect(formatterSpy).toHaveBeenCalledTimes(1);
            expect(formatterSpy).toHaveReturnedWith(formattedResults);
        });

        it('should generate page HTML reports', async () => {
            expect.assertions(2);

            const { generatePageReportSpy } = await runReporterWithMocks();

            expect(generatePageReportSpy).toHaveBeenCalledTimes(1);
            expect(generatePageReportSpy).toHaveBeenCalledWith(
                formattedResults.results,
                defaultOptions
            );
        });

        it('should generate summary HTML report', async () => {
            expect.assertions(2);

            const { generateSummaryReportSpy } = await runReporterWithMocks();

            expect(generateSummaryReportSpy).toHaveBeenCalledTimes(1);
            expect(generateSummaryReportSpy).toHaveBeenCalledWith(
                expect.objectContaining({
                    date: expect.any(Date),
                    pages: pageSummary
                }),
                defaultOptions.destination
            );
        });

        it('should log error to console if thrown', async () => {
            expect.assertions(2);

            const { consoleWarnSpy } = await runReporterWithMocks({}, true);

            expect(consoleWarnSpy).toHaveBeenCalledTimes(1);
            expect(consoleWarnSpy).toHaveBeenCalledWith(
                'Error creating HTML report - Ouch'
            );
        });
    });
});
