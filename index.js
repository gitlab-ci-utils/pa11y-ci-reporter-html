'use strict';

/**
 * Pa11y CI HTML reporter generates HTML reports for Pa11y CI, including
 * a summary HTML report and detailed page HTML reports.
 *
 * @module pa11y-ci-reporter-html
 */

const formatter = require('./lib/data-formatter');
const logger = require('ci-logger');
const reporter = require('./lib/reporter-html');
const defaultOptions = require('./lib/default-options');

/**
 * Identify compatible versions of pa11y-ci, even though not formally part of
 * the pa11y-ci reporter interface.
 */
const supports = '^3.0.0 || ^4.0.0';

/**
 * Factory function that creates a Pa11y CI HTML reporter, which generates
 * a summary HTML report and detailed page HTML reports.
 *
 * @param   {object} options The reporter options from configuration.
 * @returns {object}         The pa11y-ci reporter.
 * @static
 */
const reporterFactory = (options = {}) => {
    const reportOptions = { ...defaultOptions, ...options };

    const afterAll = async (results) => {
        try {
            reporter.ensureOutputDirectory(reportOptions.destination);
            const formattedResults = formatter.getResultsForHtmlReport(results);
            const pages = await reporter.generatePageHtmlReports(
                formattedResults.results,
                reportOptions
            );
            reporter.generateSummaryHtmlReport(
                { date: new Date(), pages, summary: formattedResults.summary },
                reportOptions.destination
            );
        } catch (error) {
            logger.log({
                level: logger.Levels.Warn,
                message: `Error creating HTML report - ${error.message}`
            });
        }
    };

    return {
        afterAll,
        supports
    };
};

module.exports = reporterFactory;
