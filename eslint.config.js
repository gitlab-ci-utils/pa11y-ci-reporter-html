'use strict';

const recommendedConfig = require('@aarongoldenthal/eslint-config-standard/recommended');

module.exports = [
    ...recommendedConfig,
    {
        files: ['playwright.config.js'],
        rules: {
            'jsdoc/imports-as-dependencies': 'off'
        }
    },
    {
        ignores: ['.vscode/**', 'archive/**', 'node_modules/**', 'coverage/**'],
        name: 'ignores'
    }
];
